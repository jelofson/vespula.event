# README #

A simple library for binding and triggering events.

## What's new

**2.1.0**

* Added priority to event callbacks
* Ability to stop propagation
* Ability to unbind events
* New trait that you can use in any class 

## Documentation

Documentation is available at https://vespula.bitbucket.io/event/
