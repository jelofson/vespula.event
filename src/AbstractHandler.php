<?php
namespace Vespula\Event;

/**
 * Abstract class for event handling.
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
abstract class AbstractHandler implements HandlerInterface {

    /**
     *
     * @var array Callbacks keyed on class name and event name
     */
    protected $callbacks;
    
    /**
     * Store the callbacks here. Can use this to test for presence of callback
     * 
     * @var type SplObjectStore
     */
    protected $objectStore; 

    /**
     * Get events for a given class name
     *
     * @param string $class_name The name of the class with events bound
     * @return array The events for a given class
     */
    protected function getEvents($class_name)
    {
        if (array_key_exists($class_name, $this->callbacks)) {
            return $this->callbacks[$class_name];
        }
        return [];
    }

    /**
     * Get the callbacks for a given event name.
     *
     * @see getCallbacksByClass()
     * @param string $event The event name
     * @param array $events The array of events in the given class
     * @return array The callbacks for an event name
     */
    protected function getCallbacks($event, $events)
    {
        if (array_key_exists($event, $events)) {
            return $events[$event];
        }
        return new SplPriorityQueue();
    }
    
    /**
     * Get the object store
     * 
     * @return SplObjectStore
     */
    protected function getObjectStore()
    {
        return $this->objectStore;
    }
    
    /**
     * Rebuild the priority queue after modifying the SplObjectStore (after unbind)
     * @param SplObjectStore $store
     * @param string $class_name
     * @param string $event_name
     */
    protected function buildPriorityQueue($store, $class_name, $event_name)
    {
        $this->callbacks[$class_name][$event_name] = new SplPriorityQueue();

        $store->rewind();
        while ($store->valid()) {
            $object = $store->current();
            $data = $store->getInfo();
            $this->callbacks[$class_name][$event_name]->insert($object, $data);
            $store->next();
        }
    }
}
