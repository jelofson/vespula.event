<?php
namespace Vespula\Event;

/**
 * Trait for event handling
 * 
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

trait EventHandlerTrait
{
    /**
     *
     * @var array of event callbacks
     */
    protected $callbacks = [];
    
    protected $store;
    

    /**
     * Bind and event to the object using the trait
     * 
     * @param string $event
     * @param callable $callback
     * @param int $priority
     */
    public function on($event, callable $callback, $priority = 0)
    {
        if (! isset($this->callbacks[$event])) {
            $this->callbacks[$event] = new SplPriorityQueue();
            $this->store[$event] = new \SplObjectStorage();
        }
        $this->callbacks[$event]->insert($callback, $priority);
        $this->store[$event]->attach($callback, $priority);
    }
    
    /**
     * Remove all callbacks for an event or just one
     * 
     * @param string $event The event name
     * @param callable $callback The optional callback (variable)
     * @return boolean or void
     */
    public function remove($event, callable $callback = null)
    {
        if (! isset($this->store[$event])) {
            return false;
        }
        
        $objectStore = $this->store[$event];

        if ($callback) {
            $exists = $objectStore->contains($callback);

            if ($exists) {
                $objectStore->detach($callback);
                $this->buildPriorityQueue($objectStore, $event);
                return true;
            }
            return false;
        } 
            
        $objectStore = new \SplObjectStorage();
        $this->buildPriorityQueue($objectStore, $event);
        return true;
        
    }
    
    
    /**
     * Trigger an event passing optional data
     * 
     * @param string $event
     * @param array $args
     * @return null
     * @throws Exception
     */
    public function trigger($event, ...$args)
    {
        switch (true) {
            case is_string($event) :
                $event_name = $event;
                $event = new Event($event_name);
            break;
            case $event instanceof Event :
                $event_name = $event->getName();
            break;
            default :
                throw new Exception('$event must be string or object of type Vespula\Event\Event');
        }

        if (! isset($this->callbacks[$event_name])) {
            return;
        }

        $callbacks = $this->callbacks[$event_name];
        if (count($callbacks) == 0) {
            return;
        }

        $callbacks->top();
        $args[] = $event;

        foreach ($callbacks as $callback) {
            call_user_func_array($callback, $args);
            if ($event->isStopped()) {
                break;
            }
        }
    }
    
    /**
     * Rebuild the priority queue after modifying the SplObjectStore (after unbind)
     * @param SplObjectStore $store
     * @param string $event
     */
    protected function buildPriorityQueue($store, $event)
    {
        $this->callbacks[$event] = new SplPriorityQueue();

        $store->rewind();
        while ($store->valid()) {
            $object = $store->current();
            $data = $store->getInfo();
            $this->callbacks[$event]->insert($object, $data);
            $store->next();
        }
    }

}
