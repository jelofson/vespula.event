<?php
namespace Vespula\Event;

/**
 * This is a very simple, but effective event handler.
 *
 * Basic usage:
 *
 * $handler = new \Vespula\Event\EventHandler;
 * $handler->bind($someObject, 'eventName', function($name) {
 *     echo $name;
 * });
 *
 * $handler->trigger($someObject, 'eventName', 'Joe');
 *
 * // Will print Joe
 * OR
 * $handler->bind(\My\Object::class, 'eventName', function($name) {
 *     echo $name;
 * });
 *
 * $handler->trigger(\My\Object::class, 'eventName', 'Joe');
 *
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class EventHandler extends AbstractHandler {



    /**
     * Bind an event to a callback. Events are keyed by class name and event.
     * You can bind multiple callbacks to the same event on a class.
     *
     * @param object|string $class The class to bind the callback to
     * @param string $event The name of the event
     * @param closure|callable $callback The code to execute when the event is triggered
     * @throws \InvalidArgumentException
     */
    public function bind($class, $event, callable $callback, $priority = 0)
    {
        $event = strtolower($event);

        $class_name = $class;
        if (is_object($class)) {
            $class_name = get_class($class);
        }

        if (! isset($this->callbacks[$class_name][$event])) {
            $this->callbacks[$class_name][$event] = new SplPriorityQueue();
        }

        if (! isset($this->objectStore[$class_name][$event])) {
            $this->objectStore[$class_name][$event] = new \SplObjectStorage();

        }

        $this->callbacks[$class_name][$event]->insert($callback, $priority);
        $this->objectStore[$class_name][$event]->attach($callback, $priority);

    }

    /**
     * Unbind (remove) an event from the queue. If you pass a callable, you can 
     * remove just it from the queue. The callable must be defined as a variable.
     * 
     * $callback = function() {// do stuff};
     * 
     * $handler->bind($class, 'myevent', $callback);
     * 
     * $handler->unbind($class, 'myevent', $callback);
     * 
     * 
     * @param string|object $class
     * @param string $event
     * @param callable $callback
     */
    public function unbind($class, $event, callable $callback = null)
    {
        $class_name = $class;
        if (is_object($class)) {
            $class_name = get_class($class);
        }
        $objectStore = $this->objectStore[$class_name][$event];

        if (isset($callback) && is_callable($callback)) {
            $exists = $objectStore->contains($callback);

            if ($exists) {
                $objectStore->detach($callback);
                $this->buildPriorityQueue($objectStore, $class_name, $event);
            }

        } else {
            $objectStore = new \SplObjectStorage();
            $this->buildPriorityQueue($objectStore, $class_name, $event);
        }
    }

    /**
     * Trigger the event
     *
     * @param object $class The class bound to the event
     * @param string $event The event name
     */
    public function trigger($class, $event, ...$args)
    {
        // TODO: check valid object
        if ($event instanceof Event) {
            $event_name = $event->getName();
        }

        if (is_string($event)) {
            // Create new event object
            $event_name = strtolower($event);
            $event = new Event($event);
        }

        $class_name = $class;
        if (is_object($class)) {
            $class_name = get_class($class);
        }

        $events = $this->getEvents($class_name);

        $callbacks = $this->getCallbacks($event_name, $events);
        if (count($callbacks) == 0) {
            return;
        }
        $callbacks->top();

        $args[] = $event;
        foreach ($callbacks as $callback) {
            call_user_func_array($callback, $args);
            if ($event->isStopped()) {
                break;
            }
        }
    }
}