<?php

namespace Vespula\Event;

/**
 * Basic event object
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
class Event 
{
    /**
     *
     * @var string Event name
     */
    protected $name;
    
    /**
     *
     * @var bool Has the event been stopped?
     */
    protected $stopped = false;
    
    /**
     * Constructor 
     * 
     * @param string $name Event name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    /**
     * Get the event name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Stop event propagation
     */
    public function stopPropagation()
    {
        $this->stopped = true;
    }
    
    /**
     * Check if the stop propagation flag is set to true
     * 
     * @return bool True of stopped
     */
    public function isStopped()
    {
        return $this->stopped;
    }
}

