<?php
namespace Vespula\Event;

// Thank you matthew weier o'phinney for your article on ordering items in
// SplPriorityQueue
//
// https://mwop.net/blog/253-Taming-SplPriorityQueue.html

/**
 * Modified priority queue that keeps track of order if priority is the same (FIFO)
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
class SplPriorityQueue extends \SplPriorityQueue
{
    /**
     * A way to keep track of secondary ordering when the priority is the same.
     * @var int
     */
    protected $queueOrder = PHP_INT_MAX;
    
    /**
     * Insert data into the queue
     * 
     * @param callable $datum
     * @param int $priority
     */
    public function insert($datum, $priority)
    {
        if (is_int($priority)) {
            $priority = array($priority, $this->queueOrder--);
        }
        parent::insert($datum, $priority);
    }
}
