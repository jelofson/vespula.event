<?php
namespace Vespula\Event;
/**
 * Event handler interface
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
interface HandlerInterface {

    /**
     * Bind and event to a class
     *
     * @param object|string $class The object to bind the event/callback to. Can use a string too.
     * @param string $event The event name
     * @param closure|callable $callback The code to execute when the event is triggered
     */
    public function bind($class, $event, callable $callback, $priority = 0);
    
    /**
     * Unbind and event to a class
     *
     * @param object|string $class The object to unbind the event/callback to. Can use a string too.
     * @param string $event The event name
     * @param callable $callback The callback to remove, if any. If none, remove all
     */
    public function unbind($class, $event, callable $callback = null);

    /**
     * Trigger the event
     *
     * @param object|string $class The class bound to the event
     * @param string $event The event name
     */
    public function trigger($class, $event);
}
