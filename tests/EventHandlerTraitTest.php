<?php
namespace Vespula\Event;

use Vespula\Event\Foo;

class EventHandlerTraitTest extends \PHPUnit_Framework_TestCase
{
    protected $foo;

    public function setUp()
    {
        $this->foo = new Foo();
    }

    public function testOn()
    {
        $this->expectOutputString('Record 4 inserted');
        $this->foo->insert();
    }

    public function testOnEvent()
    {
        $this->expectOutputString('update');
        $this->foo->update();
    }
    
    public function testRemoveOne()
    {
        $this->expectOutputString('delete');
        $this->foo->delete();
    }
    
    public function testRemoveAll()
    {
        $this->expectOutputString('');
        $this->foo->deleteAll();
    }
    
    public function testStopPropagation() 
    {
        $this->expectOutputString('stop');
        $this->foo->stop();
    }
    
}
