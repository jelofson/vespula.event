<?php

namespace Vespula\Event;

class Invokable
{
    protected $event;
    public function __invoke($string)
    {
        echo "hello " . $string;
    }
}
