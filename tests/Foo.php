<?php

namespace Vespula\Event;

class Foo
{
    use EventHandlerTrait;

    public function __construct()
    {
        $this->registerEvents();
    }

    protected function registerEvents()
    {
        $this->on('insert', function ($id) {
            echo "Record $id inserted";
            
        });

        $this->on('update', function ($event) {
            echo $event->getName();
        });
        
        $callback = function () {
            echo "Foobar";
        };
        
        $this->on('delete', $callback);
        $this->remove('delete', $callback);
        
        $this->on('delete', function ($event) {
            echo $event->getName();
        });
        
        $this->on('stop', function ($event) {
            echo $event->getName();
            $event->stopPropagation();
        });
        
        $this->on('stop', function ($event) {
            echo $event->getName();
        });

    }

    public function insert()
    {
        $this->trigger('insert', 4);
    }

    public function update()
    {
        $this->trigger('update');
    }
    
    public function delete()
    {
        $this->trigger('delete');
    }
    
    public function deleteAll()
    {
        $this->remove('delete');
        $this->trigger('delete');
    }
    
    public function stop()
    {
        $this->trigger('stop');
    }
    
}
