<?php
namespace Vespula\Event;

use Vespula\Event\EventHandler;
use Vespula\Event\Invokable;

class EventHandlerTest extends \PHPUnit_Framework_TestCase
{

    protected $handler;

    public function setUp()
    {
        $this->handler = new EventHandler;

    }

    public function testBindAndTrigger()
    {
        $this->handler->bind($this, 'event', function ($s, $r) {
            echo 'hello '. $s . ' hello ' . $r;
        });

        $this->expectOutputString('hello world hello dog');
        $this->handler->trigger($this, 'event', 'world', 'dog');
    }

    public function testBindAndTriggerCb()
    {
        $cb = function ($s, $r) {
            echo 'hello '. $s . ' hello ' . $r;
        };
        $this->handler->bind($this, 'event', $cb);

        $this->expectOutputString('hello world hello dog');
        $this->handler->trigger($this, 'event', 'world', 'dog');
    }
    
    public function testUnbindCallback()
    {
        $callback = function ($s) {
            echo 'hello '. $s;
        };
        
        
        $this->handler->bind($this, 'event', $callback);
        $this->handler->bind($this, 'event', function () {
            echo 'foobar';
        });
        $this->handler->unbind($this, 'event', $callback);

    }
    
    public function testUnbindAll()
    {
        $callback = function ($s) {
            echo 'hello '. $s;
        };


        $this->handler->bind($this, 'event', $callback);
        $this->handler->bind($this, 'event', function () {
            echo 'foobar';
        });
        $this->handler->unbind($this, 'event');

        $this->expectOutputString('');
        $this->handler->trigger($this, 'event', 'world');
    }

    public function testInvokable()
    {
        $invokable = new Invokable;
        $this->handler->bind($this, 'event', $invokable);

        $this->expectOutputString('hello world');

        $this->handler->trigger($this, 'event', 'world');
    }


    public function testMultipleCallbacks()
    {
        $this->handler->bind($this, 'event', function ($s) {
            echo "Hello $s. ";
        });
        $this->handler->bind($this, 'event', function ($s) {
            echo "I live on the $s.";
        });

        $this->expectOutputString('Hello world. I live on the world.');
        $this->handler->trigger($this, 'event', 'world');

    }

    public function testNoArgs()
    {
        $this->handler->bind($this, 'event', function () {
            echo 'hello';
        });
        $this->expectOutputString('hello');
        $this->handler->trigger($this, 'event');
    }

    public function testBindString()
    {
        $this->handler->bind(EventHandlerTest::class, 'event', function ($s, $r) {
            echo 'hello '. $s . ' hello ' . $r;
        });

        $this->expectOutputString('hello world hello dog');
        $this->handler->trigger($this, 'event', 'world', 'dog');
    }

    public function testPriority()
    {
        // priority 0
        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo 'priority 0 ';
        });

        // priority 5
        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo 'priority 5 ';
        }, 5);

        // priority 10
        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo 'priority 10 ';
        }, 10);



        $expected = 'priority 10 priority 5 priority 0 ';

        $this->handler->trigger($this, 'event');

        $this->expectOutputString($expected);
    }

    public function testOrderSamePriority()
    {
        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo '1';
        }, 2);

        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo '2';
        }, 2);

        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo '3';
        }, 2);

        $expected = '123';

        $this->handler->trigger($this, 'event');

        $this->expectOutputString($expected);
    }

    public function testStopPropagation()
    {
        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo 'I will run! ';
        });

        $this->handler->bind(EventHandlerTest::class, 'event', function ($e) {
            echo 'I will run too';
            $e->stopPropagation();
        });

        $this->handler->bind(EventHandlerTest::class, 'event', function () {
            echo 'Should not run';
        });

        $expected = 'I will run! I will run too';

        $this->handler->trigger($this, 'event');

        $this->expectOutputString($expected);
    }
    
    public function testEventLastArg()
    {
        $this->handler->bind($this, 'event', function ($s, $r, $e) {
            if ($e instanceof Event) {
                echo $s . $r;
            }
        });

        $this->expectOutputString('foobar');
        $this->handler->trigger($this, 'event', 'foo', 'bar');
    }
    
    public function testEventTarget()
    {
        $this->handler->bind($this, 'event', function ($e) {
            echo $e->getName();
        });

        $this->expectOutputString('event');
        $this->handler->trigger($this, 'event');
    }

    public function testUnbind()
    {


        $this->handler->bind($this, 'event', function () {
            echo 'foo';
        });

        $this->handler->bind($this, 'event', function () {
            echo 'bar';
        });

        $this->handler->unbind($this, 'event');

        $this->expectOutputString('');
        $this->handler->trigger($this, 'event');

    }



}
